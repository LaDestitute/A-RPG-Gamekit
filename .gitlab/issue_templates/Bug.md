/////////Use this template to submit issues and use 'X' in checkboxes as with the examples below. Omit any text surrounded with slashes, so this too. Before submitting, put the Operating System line as the first line of the submitted issue.////////

**Operating System:**  
- [ ] Windows (32-bit)
- [X] Windows (64-bit)
- [ ] OSX (64-bit)
- [ ] Linux (32-bit)
- [ ] Linux (64-bit)

**Select Bug Category:**  
- [ ] Audio
- [ ] Controls
- [ ] Dialogue
- [ ] Dungeon
- [ ] Enemy AI
- [ ] File Select
- [ ] Graphical
- [ ] Items
- [ ] Inventory
- [ ] Objects/World
- [ ] Player
- [ ] Savedata

**Bug Status:**
- [ ] Gamebreaking/Crashes/Softlocks/etc
- [ ] Critical, while not gamebreaking or crash inducing, may majorly impact functionality, playability, etc
- [ ] Non-critical (no major negative or functional impact, just an ignorable quirk or otherwise anomalous behavior)

**Description (describe accurately and as best as you can):**  
////////Include some standard expected info, so examples of said standard info: were you able to reproduce the bug multiple times, is the bug an edge-case, does it break the game, cause a freeze/hang-up, cause a softlock, does it make nw.js crash, etc.////////