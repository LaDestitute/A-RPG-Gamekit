# A-RPG-Gamekit
Further info:

https://www.scirra.com/forum/action-rpg-gamekit-wip_t169296

Join the discord server for the gamekit here:

https://discord.gg/E7npzRN

My discord server has a webhook-based bot that posts notifications for whenever a commit/issue/etc is made.

This is an action-RPG gamekit developed in Construct 2. Most of the gameplay is in the style of classic 2D Legend of Zelda, such as A Link to the Past and Link's Awakening but inspiration is also being taken from games such as Alundra and Secret of Mana.

Please keep note that most of the development changes occur on the unstable branch, and so things may be unfinished, have bugs, use quick lazy shortcut methods, etc.
For bug reports, please submit them to the Issues part of the repo and be sure to use tags properly, please. Tag them based on area (inventory, combat, etc) and by further subsections, such as tag the item or enemy it occurs with.

This repo is intended for the purpose of to track changes made by me more officially, and on a small note, keep note that the online commit-timestamps are UTC based while my local timezone is Pacific Standard Time.

Also, a roadmap Trello here:

https://trello.com/c/jRK00Nkj/27-add-an-in-engine-starter-map-ala-pokemon-essential-s-shows-examples-for-stuff-like-dialogue-and-shops